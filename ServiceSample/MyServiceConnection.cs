using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ServiceSample
{
    public class MyServiceConnection : Java.Lang.Object, IServiceConnection
    {
        private MyService myService = null;
        private bool isBound = false;
        
        public MyServiceConnection()
        {
            
        }

        public void OnServiceConnected(ComponentName componentName, IBinder service)
        {
            isBound = true;

            MyBinder myBinder = (MyBinder)service;
            myService = myBinder.GetService();

            if (myService != null)
            {
                CustomToast.ShowToastByHandler(myService.ApplicationContext, "onServiceConnected", 0);
            }
        }

        public void OnServiceDisconnected(ComponentName componentName)
        {
            isBound = false;

            CustomToast.ShowToastByHandler(myService.ApplicationContext, "onServiceDisconnected", 0);
        }

        public bool IsBound()
        {
            return isBound;
        }
    }
}