using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Util;

namespace ServiceSample
{
    [Service]
    public class MyService :Service
    {
        private bool stopThread = false;
        public const String STOP_THREAD = "stopThread";
        private Thread thread = null;

        public MyService()
        {
            
        }

        public override void OnCreate(){
            base.OnCreate();

            CustomToast.ShowToastByHandler(ApplicationContext, "OnCreate", 0);

            stopThread = false;
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startID)
        {
            CustomToast.ShowToastByHandler(ApplicationContext, "OnStartCommand", 0);

            stopThread = intent.GetBooleanExtra(STOP_THREAD, false);

            thread = new Thread(() =>
            {
                while (stopThread == false)
                {
                    CustomToast.ShowToastByHandler(ApplicationContext, "SERVICE RUNNING", CustomToast.TEN_SECONDS);                    
                }

                if (stopThread)
                {
                    CustomToast.ShowToastByHandler(ApplicationContext, "THREAD STOPPED", 0);
                }

            });
            thread.Start();
            
            return base.OnStartCommand(intent, flags, startID);
        }

        public override IBinder OnBind(Intent intent)
        {
            CustomToast.ShowToastByHandler(ApplicationContext, "OnBind", 0);
            
            MyBinder binder = new MyBinder(this);

            return binder;
        }

        public override void OnDestroy()
        {
            stopThread = true;

            if (thread != null)
            {
                thread = null;
            }

            CustomToast.ShowToastByHandler(ApplicationContext, "OnDestroy", 0);

            base.OnDestroy();            
        }

         
        public override bool OnUnbind(Intent intent) {
            CustomToast.ShowToastByHandler(ApplicationContext, "OnUnbind", 0);

            return false;
        }
    }
}