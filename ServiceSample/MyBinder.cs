using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ServiceSample
{
    public class MyBinder : Binder
    {
        private MyService service = null;

        public MyBinder(MyService service)
        {
            this.service = service;

            CustomToast.ShowToastByHandler(service.ApplicationContext, "MyBinder", 0);            
        }

        public MyService GetService()
        {
            CustomToast.ShowToastByHandler(service.ApplicationContext, "GetService", 0);

            return service;
        }
    }
}