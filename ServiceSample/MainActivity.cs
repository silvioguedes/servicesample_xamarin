﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Util;

namespace ServiceSample
{
    [Activity(Label = "ServiceSample", MainLauncher = true, Icon = "@drawable/icon")]
    public class MainActivity : Activity
    {
        private Button startServiceButton = null;
        private Button stopServiceButton = null;
        private Button stopThreadButton = null;

        private Button startIntentServiceButton = null;
        private Button stopIntentServiceButton = null;
        private Button stopHandleIntentButton = null;

        private MyIntentServiceConnection intentServiceConnection = null;
        private MyServiceConnection serviceConnection = null;
        private Intent intentForService = null;
        private Intent intentForIntentService = null;
        
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);            
            SetContentView(Resource.Layout.Main);

            intentForService = new Intent(this, typeof(MyService));
            serviceConnection = new MyServiceConnection();
            intentForIntentService = new Intent(this, typeof(MyIntentService));
            intentServiceConnection = new MyIntentServiceConnection();

            startServiceButton = FindViewById<Button>(Resource.Id.startService);
            startServiceButton.Click += delegate {
                intentForService.PutExtra(MyService.STOP_THREAD, false);
                StartService(intentForService);
            };            

            stopThreadButton = FindViewById<Button>(Resource.Id.stopThread);
            stopThreadButton.Click += delegate
            {                
                intentForService.PutExtra(MyService.STOP_THREAD, true);
                StartService(intentForService);
            };

            stopServiceButton = FindViewById<Button>(Resource.Id.stopService);
            stopServiceButton.Click += delegate
            {                
                StopService(intentForService);
            };


            startIntentServiceButton = FindViewById<Button>(Resource.Id.startIntentService);
            startIntentServiceButton.Click += delegate
            {                
                intentForIntentService.PutExtra(MyIntentService.STOP_HANDLE_INTENT, false);
                StartService(intentForIntentService);
            };
            
            stopHandleIntentButton = FindViewById<Button>(Resource.Id.stopHandleIntent);
            stopHandleIntentButton.Click += delegate
            {                
                intentForIntentService.PutExtra(MyIntentService.STOP_HANDLE_INTENT, true);
                StartService(intentForIntentService);
            };

            stopIntentServiceButton = FindViewById<Button>(Resource.Id.stopIntentService);
            stopIntentServiceButton.Click += delegate
            {             
                StopService(intentForIntentService);
            };
        }

        protected override void OnResume()
        {
            if (intentForService != null && serviceConnection != null)
            {
                BindService(intentForService, serviceConnection, 0);
            }

            if (intentForIntentService != null && intentServiceConnection != null)
            {
                BindService(intentForIntentService, intentServiceConnection, 0);
            }
            
            base.OnResume();
        }

        protected override void OnPause()
        {
            if (serviceConnection != null)
            {
                UnbindService(serviceConnection);
            }

            if (intentServiceConnection != null)
            {
                UnbindService(intentServiceConnection);
            }

            base.OnResume();
        }
    }            
}

