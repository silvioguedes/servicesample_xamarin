package md5864291a86406e1c9dc6af37c1ebee451;


public class MyIntentBinder
	extends android.os.Binder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("ServiceSample.MyIntentBinder, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MyIntentBinder.class, __md_methods);
	}


	public MyIntentBinder () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyIntentBinder.class)
			mono.android.TypeManager.Activate ("ServiceSample.MyIntentBinder, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public MyIntentBinder (md5864291a86406e1c9dc6af37c1ebee451.MyIntentService p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyIntentBinder.class)
			mono.android.TypeManager.Activate ("ServiceSample.MyIntentBinder, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "ServiceSample.MyIntentService, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
