package md5864291a86406e1c9dc6af37c1ebee451;


public class MyBinder
	extends android.os.Binder
	implements
		mono.android.IGCUserPeer
{
/** @hide */
	public static final String __md_methods;
	static {
		__md_methods = 
			"";
		mono.android.Runtime.register ("ServiceSample.MyBinder, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", MyBinder.class, __md_methods);
	}


	public MyBinder () throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyBinder.class)
			mono.android.TypeManager.Activate ("ServiceSample.MyBinder, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "", this, new java.lang.Object[] {  });
	}

	public MyBinder (md5864291a86406e1c9dc6af37c1ebee451.MyService p0) throws java.lang.Throwable
	{
		super ();
		if (getClass () == MyBinder.class)
			mono.android.TypeManager.Activate ("ServiceSample.MyBinder, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "ServiceSample.MyService, ServiceSample, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", this, new java.lang.Object[] { p0 });
	}

	private java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
