using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ServiceSample
{
    [BroadcastReceiver]
    [IntentFilter(new String[] { "android.intent.action.BOOT_COMPLETED" })]
    public class MyBroadcastReceiverForIntentService : BroadcastReceiver
    {
        public MyBroadcastReceiverForIntentService()
        {
            
        }

        public override void OnReceive(Context context, Intent intent)
        {
            CustomToast.ShowToastByHandler(context, "OnReceive", 0);
            
            Intent newIntent = new Intent(context,typeof(MyIntentService));
            newIntent.PutExtra(MyIntentService.STOP_HANDLE_INTENT, false);
            context.StartService(newIntent);
        }
    }
}