using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Util;

namespace ServiceSample
{
    public class MyIntentBinder : Binder
    {
        private MyIntentService service = null;

        public MyIntentBinder(MyIntentService service)
        {
            CustomToast.ShowToastByHandler(service.ApplicationContext, "MyIntentBinder", 0);

            this.service = service;
        }

        public MyIntentService GetService()
        {
            CustomToast.ShowToastByHandler(service.ApplicationContext, "GetService", 0);

            return service;
        }
    }
}