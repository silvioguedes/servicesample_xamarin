using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Threading;
using Android.Util;

namespace ServiceSample
{
    [Service]
    public class MyIntentService : IntentService
    {
        private bool stopHandleIntent = false;
        public const String STOP_HANDLE_INTENT = "stopHandleIntent";
        private Handler mHandler = new Handler();
        
        public MyIntentService()
        {
            
        }

        public override void OnCreate(){
            base.OnCreate();

            CustomToast.ShowToastByHandler(ApplicationContext, "onCreate", 0);
        }

        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startID)
        {
            if (intent != null)
            {
                CustomToast.ShowToastByHandler(ApplicationContext, "OnStartCommand", 0);

                stopHandleIntent = intent.GetBooleanExtra(STOP_HANDLE_INTENT, false);
            }
                   
            return base.OnStartCommand(intent, flags, startID);
        }

        public override IBinder OnBind(Intent intent)
        {
            if (intent != null)
            {
                CustomToast.ShowToastByHandler(ApplicationContext, "OnBind", 0);
            }

            MyIntentBinder binder = new MyIntentBinder(this);

            return binder;
        }

        protected override void OnHandleIntent(Intent intent){
            if (intent != null){
                CustomToast.ShowToastByHandler(ApplicationContext, "OnHandleIntent", 0);
            }

            while (stopHandleIntent == false)
            {
                CustomToast.ShowToastByHandler(ApplicationContext, "SERVICE RUNNING", CustomToast.TEN_SECONDS);
            }

            if (stopHandleIntent)
            {
                CustomToast.ShowToastByHandler(ApplicationContext, "HANDLE STOPPED", 0);
            }
        }

        public override void OnDestroy()
        {
            stopHandleIntent = true;

            CustomToast.ShowToastByHandler(ApplicationContext, "OnDestroy", 0);

            base.OnDestroy();            
        }

        public override bool OnUnbind(Intent intent)
        {
            CustomToast.ShowToastByHandler(ApplicationContext, "OnUnbind", 0);

            return false;
        }
    }
}